#!/bin/bash
# Helper script to run other extraction tasks
# Input argument is output format for image files (png or jpg)

if [ "$#" -ne 2 ]; then
	echo "This script takes 1 parameter, the path to the data directory, e.g.,"
	echo "./scripts/extractAll.sh ./sampleData" 
	echo "Optionally you can also specify the image format:"
	echo "./scripts/extractAll.sh ./sampleData png" 
fi

# Figure out the path of helper scripts
scriptParentFolder=$1

datasetName=$2

# Format for extracted images.
# Use png for best quality.
fmt=${3-jpg}

cd $scriptParentFolder/$datasetName

# Extract skeletons
if [ -f vgaPose3d_stage1.tar ]; then
	tar -xf vgaPose3d_stage1.tar
fi

if [ -f hdPose3d_stage1.tar ]; then
	tar -xf hdPose3d_stage1.tar
fi



# Extract VGA images
$scriptParentFolder/scripts/vgaImgsExtractor.sh ${fmt}

# Extract HD images
$scriptParentFolder/scripts/hdImgsExtractor.sh ${fmt}

cd $scriptParentFolder
